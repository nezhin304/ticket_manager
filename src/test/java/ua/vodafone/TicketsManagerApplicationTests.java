package ua.vodafone;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.vodafone.entity.Carrier;
import ua.vodafone.entity.Tgno;
import ua.vodafone.service.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TicketsManagerApplicationTests {

    @Autowired
    CarrierService carrierService;
    @Autowired
    TgnoService tgnoService;
    @Autowired
    TicketsService ticketsService;
    @Autowired
    CustomerService customerService;
    @Autowired
    RoleService roleService;


    @Test
    public void contextLoads() throws ParseException {

//        Carrier carrier = new Carrier();
//        carrier.setName("3Mob Ukraine");
//        carrier.setEmail("Znk.mcsc@a1telekom.at");
//        carrier.setOtherInformations("Phone +54545455454, Petja");
//
//        Tgno tgno = new Tgno();
//        tgno.setName("BAUSV1");
//        tgno.setTechnology("TDM");
//        tgno.setDpc("4608");
//        tgno.setOpc("14222");
//        tgno.setCarrier(carrier);
//
//        Ticket ticket = new Ticket();
//        ticket.setDateOpen(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-07-29 12:00:00"));
//        ticket.setTgno(tgno);
//        ticket.setDest("RUSSIA");
//
//        carrierService.save(carrier);
//        tgnoService.save(tgno);
//        ticketsService.save(ticket);

    }

//    @Test
//    public void fillTheTableCarriers() throws IOException {
//        String fileName = "C:\\Users\\akuzmenko\\Desktop\\Operators Info_IGW Huawei_ISC EWSD.xlsx";
//
//        FileInputStream fileInputStream = new FileInputStream(new File(fileName));
//        XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
//        XSSFSheet sheet = workbook.getSheetAt(0);
//        int numOfPhysRows = sheet.getPhysicalNumberOfRows();
//        String carrierName = "";
//        String carrierOtherInformations = "";
//
//        for (int i = 1; i < numOfPhysRows; i++) {
//
//            XSSFRow row = sheet.getRow(i);
//
//            Carrier carrier = new Carrier();
//
//            if (!row.getCell(1).getStringCellValue().equals("")) {
//                carrierName = row.getCell(1).getStringCellValue();
//                carrier.setName(carrierName);
//            } else {
//                carrier.setName(carrierName);
//            }
//
//            if (!row.getCell(13).getStringCellValue().equals("")) {
//                carrierOtherInformations = row.getCell(13).getStringCellValue();
//                carrier.setOtherInformations(carrierOtherInformations);
//            } else {
//                carrier.setOtherInformations(carrierOtherInformations);
//            }
//
//            try {
//                carrierService.save(carrier);
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//
//
//            if (row.getCell(3).getStringCellValue().equals("TDM")) {
//
//                Tgno tgno = new Tgno();
//                tgno.setCallSource(String.valueOf((int) row.getCell(2).getNumericCellValue()));
//                tgno.setTechnology("TDM");
//                tgno.setName(row.getCell(5).getStringCellValue());
//                tgno.setOpc(String.valueOf((int) row.getCell(10).getNumericCellValue()));
//                tgno.setDpc(String.valueOf((int) row.getCell(11).getNumericCellValue()));
//
//                Long newCarrierId = carrierService.getCarrierIdByName(carrierName);
//                Carrier newCarrier = carrierService.get(newCarrierId);
//                tgno.setCarrier(newCarrier);
//
//                tgnoService.save(tgno);
//
//            } else {
//
//                Tgno tgno = new Tgno();
//                tgno.setCallSource(String.valueOf((int) row.getCell(2).getNumericCellValue()));
//                tgno.setTechnology("SIP");
//                tgno.setPublicOrPrivate(row.getCell(4).getStringCellValue());
//                tgno.setName(row.getCell(5).getStringCellValue());
//
//                tgno.setSigSourceIp(row.getCell(6).getStringCellValue());
//                tgno.setMediaSourceIp(row.getCell(7).getStringCellValue());
//                tgno.setSigDestinationIp(row.getCell(8).getStringCellValue());
//                tgno.setMediaDestinationIp(row.getCell(9).getStringCellValue());
//
//                Long newCarrierId = carrierService.getCarrierIdByName(carrierName);
//                Carrier newCarrier = carrierService.get(newCarrierId);
//                tgno.setCarrier(newCarrier);
//
//                tgnoService.save(tgno);
//
//            }
//        }
//    }
//
//
//    @Test
//    public void fillTheEmail() {
//
//        List<Carrier> carriers = carrierService.listAll();
//        for (Carrier carrier : carriers) {
//
//            String s = carrier.getOtherInformations();
//            String emails = "";
//
//            Matcher m = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(s);
//            while (m.find()) {
//                emails = emails + m.group() + ", ";
//            }
//
//            carrier.setEmail(emails);
//            carrierService.save(carrier);
//        }
//    }

    @Test
    public void getAllTgno() {
//        List<String> tgnoNames = tgnoService.getAllTgnoNames();
//        for (String name : tgnoNames) {
//            System.out.println(name);
//        Ticket ticket = new Ticket();
//        ticket.getTgno().getName()
//
//        }

    }

    @Test
    public void createCustomer() throws ParseException {

//        Customer customer = new Customer();
//        customer.setUserName("test");
//        customer.setPassword("test");
//        customer.setActive(true);
//
//        customerService.save(customer);
//
//        Role role = new Role();
//        role.setRole("USER");
//        role.setCustomer(customerService.getCustomerByName(customer.getName()));
//        roleService.save(role);

//        Ticket ticket = new Ticket();
//        ticket.setDateOpen(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-08-01 16:27:38"));
//        ticket.setTgno(tgnoService.getTgnoByName("SIP_IBICS2"));
//        ticket.setCustomer(customerService.getCustomerByUserName("test"));
//        ticketsService.save(ticket);
    }
//
//    @Test
//    public void checkTg() throws IOException {
//
//        String fileWithTg = "C:\\Users\\akuzmenko\\Desktop\\tgall.txt";
//        BufferedReader reader = Files.newBufferedReader(Paths.get(fileWithTg));
//        List<String> listTg = new ArrayList<>();
//        String line;
//        while ((line = reader.readLine()) != null) {
//            if (line.contains(" SIP ") | line.contains(" ISUP ")) {
//                line = line.replaceAll("\\s+ISUP\\s+.+", "");
//                line = line.replaceAll("\\s+SIP\\s+.+", "");
//                line = line.replaceAll("\\s", "");
//                if (!listTg.contains(line)) {
//                    listTg.add(line);
//                }
//            }
//        }
//
//        List<String> allTgnoNames = tgnoService.getAllTgnoNames();
//
//        for (String s : listTg) {
//            if (!allTgnoNames.contains(s)) {
//                System.out.println(s);
//            }
//        }
//    }
}
