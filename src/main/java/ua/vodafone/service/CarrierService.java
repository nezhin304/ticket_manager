package ua.vodafone.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.vodafone.entity.Carrier;
import ua.vodafone.repository.CarrierRepository;

import java.util.List;

@Service
public class CarrierService {

    @Autowired
    private CarrierRepository carrierRepository;

    public List<Carrier> listAll() {
        return carrierRepository.findAllByOrderByIdAsc();
    }

    public Carrier save(Carrier carrier) {
        return carrierRepository.save(carrier);
    }

    public Carrier get(Long id) {
        return carrierRepository.findById(id).get();
    }

    public Long getCarrierIdByName(String name) {
        return carrierRepository.getCarrierIdByName(name);
    }

    public void delete(Long id) {
        carrierRepository.deleteById(id);
    }
}
