package ua.vodafone.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.vodafone.entity.Ticket;
import ua.vodafone.repository.TicketsRepository;

import java.util.List;

@Service
public class TicketsService {

    @Autowired
    private TicketsRepository ticketsRepository;

    public List<Ticket> listAllSortByIdAsc() {
        return ticketsRepository.findAllByActiveIsTrueOrderByIdAsc();
    }

    public List<Ticket> listAllSortByIdDesc() {
        return ticketsRepository.findAllByActiveIsTrueOrderByIdDesc();
    }

    public List<Ticket> listActiveSortByIdAsc() {
        return ticketsRepository.findAllByDateCloseIsNullAndActiveIsTrueOrderByIdAsc();
    }

    public List<Ticket> listActiveSortByIdDesc() {
        return ticketsRepository.findAllByDateCloseIsNullAndActiveIsTrueOrderByIdDesc();
    }

    public Ticket save(Ticket ticket) {
        ticketsRepository.save(ticket);
        return ticket;
    }

    public Ticket get(Long id) {
        return ticketsRepository.findById(id).get();
    }

    public void delete(Long id) {
        ticketsRepository.deleteById(id);
    }

}






