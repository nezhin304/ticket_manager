package ua.vodafone.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.vodafone.entity.Customer;
import ua.vodafone.repository.CustomerRepository;

@Service
public class CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    public Customer getCustomerByUserName(String username) {
        return customerRepository.getCustomerByUserName(username);
    }

    public Customer getCustomerById(Long id) {
        return customerRepository.getCustomerById(id);
    }

    public void save(Customer customer) {
        customerRepository.save(customer);
    }

    public void delete(Customer customer) {
        customerRepository.delete(customer);
    }
}
