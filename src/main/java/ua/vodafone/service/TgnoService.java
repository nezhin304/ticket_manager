package ua.vodafone.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.vodafone.entity.Tgno;
import ua.vodafone.repository.TgnoRepository;

import java.util.Collections;
import java.util.List;

@Service
public class TgnoService {

    @Autowired
    private TgnoRepository tgnoRepository;

    public List<Tgno> listAll() {
        return tgnoRepository.findAllByOrderByIdAsc();
    }

    public Tgno save(Tgno tgno) {
        return tgnoRepository.save(tgno);
    }

    public Tgno get(Long id) {
        return tgnoRepository.findById(id).get();
    }

    public Long getTgnoIdByName(String name) {
        return tgnoRepository.getTgnoIdByName(name);
    }

    public Tgno getTgnoByName(String name) {
        return tgnoRepository.getTgnoByName(name);
    }

    public List<String> getAllTgnoNames() {
        List<String> tgnoList = tgnoRepository.getAllTgnoNames();
        Collections.sort(tgnoList);
        return tgnoList;
    }

    public void delete(Long id) {
        tgnoRepository.deleteById(id);
    }
}
