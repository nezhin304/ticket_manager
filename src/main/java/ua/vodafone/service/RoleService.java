package ua.vodafone.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.vodafone.entity.Role;
import ua.vodafone.repository.RoleRepository;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    public Role getRoleByRole(String roleName) {
        return roleRepository.getRoleByRole(roleName);
    }

    public void save(Role role) {
        roleRepository.save(role);
    }
}
