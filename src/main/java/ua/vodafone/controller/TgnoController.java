package ua.vodafone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ua.vodafone.entity.Carrier;
import ua.vodafone.entity.Tgno;
import ua.vodafone.service.CarrierService;
import ua.vodafone.service.TgnoService;

import java.util.List;

@Controller
public class TgnoController {

    @Autowired
    TgnoService tgnoService;
    @Autowired
    CarrierService carrierService;

    @GetMapping("/carrier/all")
    private String allTgno(Model model) {
        List<Tgno> tgnoList = tgnoService.listAll();
        model.addAttribute("tgnoList", tgnoList);
        return "carrier/all";
    }

    @GetMapping("/carrier/new")
    public String showNewTicketForm(Model model) {
        Tgno tgno = new Tgno();
        model.addAttribute("tgno", tgno);
        return "carrier/new";
    }

    @PostMapping("/carrier/save")
    public String saveTgno(@ModelAttribute("tgno") Tgno tgno) {

        if (carrierService.getCarrierIdByName(tgno.getCarrier().getName()) == null) {
            Carrier carrier = carrierService.save(tgno.getCarrier());
            tgno.setCarrier(carrier);
            Tgno tgno_saved = tgnoService.save(tgno);
            return "redirect:/carrier/edit/" + tgno_saved.getId();
        }

        if (carrierService.getCarrierIdByName(tgno.getCarrier().getName()) != null) {
            Carrier carrier = carrierService.get(carrierService.getCarrierIdByName(tgno.getCarrier().getName()));
            tgno.setCarrier(carrier);
            Tgno tgno_saved = tgnoService.save(tgno);
            return "redirect:/carrier/edit/" + + tgno_saved.getId();
        }

        return "redirect:/carrier/all";
    }

    @GetMapping("/carrier/edit/{id}")
    public ModelAndView showEditCarrierForm(@PathVariable(name = "id") Long id) {
        ModelAndView modelAndView = new ModelAndView("carrier/edit");
        Tgno tgno = tgnoService.get(id);
        modelAndView.addObject("tgno", tgno);
        return modelAndView;
    }
}
