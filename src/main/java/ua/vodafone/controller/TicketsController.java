package ua.vodafone.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.vodafone.entity.Ticket;
import ua.vodafone.service.CustomerService;
import ua.vodafone.service.TgnoService;
import ua.vodafone.service.TicketsService;

import java.time.ZonedDateTime;
import java.util.List;


@Controller
public class TicketsController {

    private final TicketsService ticketsService;
    private final TgnoService tgnoService;
    private final CustomerService customerService;

    public TicketsController(TicketsService ticketsService, TgnoService tgnoService, CustomerService customerService) {
        this.ticketsService = ticketsService;
        this.tgnoService = tgnoService;
        this.customerService = customerService;
    }

    @GetMapping("/tickets/all")
    private String allTickets(Model model) {
        List<Ticket> listTickets = ticketsService.listAllSortByIdDesc();
        model.addAttribute("listTickets", listTickets);
        return "ticket/all_tickets";
    }

    @GetMapping("/tickets")
    private String openTickets(Model model) {
        List<Ticket> listTickets = ticketsService.listActiveSortByIdDesc();
        model.addAttribute("listTickets", listTickets);
        return "ticket/all_tickets";
    }

    @GetMapping("/tickets/new")
    public String showNewTicketForm(Model model) {
        Ticket ticket = new Ticket();
        List<String> tgnoNames = tgnoService.getAllTgnoNames();
        model.addAttribute("ticket", ticket);
        model.addAttribute("tgnoNames", tgnoNames);
        return "ticket/new_ticket";
    }

    @PostMapping("/tickets/save")
    public String saveTicket(@ModelAttribute("ticket") Ticket ticket,
                             @RequestParam(value = "action", required = true) String action) {
        ticket.setTgno(tgnoService.getTgnoByName(ticket.getTgno().getName()));
        ticket.setCustomer(customerService.getCustomerByUserName(ticket.getCustomer().getUserName()));
//        ticketsService.save(ticket);
        Ticket saved_ticket = null;
        if (action.equals("save")) {
            saved_ticket = ticketsService.save(ticket);
        }

        if (action.equals("saveAsNew")) {
            ticket.setId(null);
            saved_ticket = ticketsService.save(ticket);
        }

        assert saved_ticket != null;
        if (ticket.getDateClose() != null) {
            return "redirect:/tickets";
        } else {
            return "redirect:/tickets/edit/" + saved_ticket.getId();
        }
    }


    @GetMapping("/tickets/edit/{id}")
    public ModelAndView showEditTicketForm(@PathVariable(name = "id") Long id) {
        ModelAndView modelAndView = new ModelAndView("ticket/edit_ticket");
        Ticket ticket = ticketsService.get(id);
        List<String> tgnoNames = tgnoService.getAllTgnoNames();
        modelAndView.addObject("ticket", ticket);
        modelAndView.addObject("tgnoNames", tgnoNames);
        return modelAndView;
    }

    @GetMapping("/tickets/delete/{id}")
    public String deleteTicket(@PathVariable(name = "id") Long id) {
        Ticket ticket = ticketsService.get(id);
        ticket.setActive(false);
        ticket.setDescription(ticket.getDescription() + " TT is deleted.");
        ticketsService.save(ticket);
        return "redirect:/tickets";
    }

    @GetMapping("/tickets/email/{id}")
    private ModelAndView showViewTicketToMail(@PathVariable(name = "id") Long id) {
        ModelAndView modelAndView = new ModelAndView("ticket/email");
        Ticket ticket = ticketsService.get(id);
        String tz = String.valueOf(ZonedDateTime.now().getOffset());
        modelAndView.addObject("ticket", ticket);
        modelAndView.addObject("tz", tz);
        return modelAndView;
    }
}
