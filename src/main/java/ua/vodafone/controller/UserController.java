package ua.vodafone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ua.vodafone.entity.Customer;
import ua.vodafone.entity.Role;
import ua.vodafone.entity.Ticket;
import ua.vodafone.service.CustomerService;
import ua.vodafone.service.RoleService;

@Controller
public class UserController {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private RoleService roleService;

    @GetMapping("/registration")
    public String showRegistrationWiev() {
        return "user/registration";
    }

    @PostMapping("/registration")
    public String addCustomer(Customer customer, Model model) {
        Customer customerFromDB = customerService.getCustomerByUserName(customer.getUserName());

        if (customerFromDB != null) {
            model.addAttribute("message", "User already exists.");
            return "user/registration";
        }

        customer.setActive(true);
        customerService.save(customer);
        Role role = new Role();
        role.setRole("USER");
        role.setCustomer(customerService.getCustomerByUserName(customer.getUserName()));
        roleService.save(role);

        return "redirect:/login";
    }

    @GetMapping("/user/edit/{username}")
    public ModelAndView showUserEditWiev(@PathVariable(name = "username") String username) {
        ModelAndView modelAndView = new ModelAndView("user/edit");
        Customer customer = customerService.getCustomerByUserName(username);
        modelAndView.addObject("customer", customer);
        return modelAndView;
    }

    @PostMapping("/user/save")
    public String updateUser(@ModelAttribute("customer") Customer customer) {
        customerService.save(customer);
        return "redirect:/tickets";
    }
}
