package ua.vodafone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ua.vodafone.entity.Carrier;
import ua.vodafone.service.CarrierService;

import java.util.List;

@Controller
public class CarrierController {

//    @Autowired
//    CarrierService carrierService;
//
//    @GetMapping("/carrier/all")
//    private String allCarriers(Model model) {
//        List<Carrier> carrierList = carrierService.listAll();
//        model.addAttribute("carrierList", carrierList);
//        return "carrier/all";
//    }
//
//    @PostMapping("/carrier/save")
//    public String saveCarrier(@ModelAttribute("carrier") Carrier carrier) {
//        carrierService.save(carrier);
//        return "redirect:/carrier/all";
//    }
//
//    @GetMapping("/carrier/edit/{id}")
//    public ModelAndView showEditCarrierForm(@PathVariable(name = "id") Long id) {
//        ModelAndView modelAndView = new ModelAndView("carrier/edit");
//        Carrier carrier = carrierService.get(id);
//        modelAndView.addObject("carrier", carrier);
//        return modelAndView;
//    }
}
