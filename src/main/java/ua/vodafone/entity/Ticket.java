package ua.vodafone.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tickets")
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dateOpen;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateClose;

    private String ttOur;
    private String ttOperator;
    private String initiator;
    private String remoteSide;
    private String problem;
    private String dest;
    private String code;
    private String testNumber;
    private String timeTestCall;
    private String causeValue;

    @Column(length = 1024)
    private String description;

    private Boolean active = true;
    private Boolean resolved = true;
    private Boolean srtBlocked = false;

    @ManyToOne
    @JoinColumn(name = "tgno_id", nullable = false)
    private Tgno tgno;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;
}
