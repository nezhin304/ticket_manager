package ua.vodafone.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tgno")
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Tgno {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String callSource;
    private String technology;
    private String publicOrPrivate;
    private String sigSourceIp;
    private String mediaSourceIp;
    private String sigDestinationIp;
    private String mediaDestinationIp;
    private String opc;
    private String dpc;

    @ManyToOne
    @JoinColumn(name = "carrier_id", nullable = false)
    private Carrier carrier;

    @Column(name = "ticket_id")
    @OneToMany
    private List<Ticket> ticketList = new ArrayList<>();
}
