package ua.vodafone.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ua.vodafone.entity.Ticket;

import java.util.List;

public interface TicketsRepository extends JpaRepository<Ticket, Long> {

    List<Ticket> findAllByActiveIsTrueOrderByIdAsc();

    List<Ticket> findAllByActiveIsTrueOrderByIdDesc();

    List<Ticket> findAllByDateCloseIsNullAndActiveIsTrueOrderByIdAsc();

    List<Ticket> findAllByDateCloseIsNullAndActiveIsTrueOrderByIdDesc();
}
