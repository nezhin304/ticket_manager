package ua.vodafone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ua.vodafone.entity.Carrier;

import java.util.List;

public interface CarrierRepository extends JpaRepository<Carrier, Long> {

    List<Carrier> findAllByOrderByIdAsc();

    @Query("select carr.id from Carrier carr where carr.name=:name")
    Long getCarrierIdByName(@Param("name") String name);

}
