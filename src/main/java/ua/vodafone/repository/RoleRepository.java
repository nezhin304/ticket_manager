package ua.vodafone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.vodafone.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role getRoleByRole(String roleName);
}
