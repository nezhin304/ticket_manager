package ua.vodafone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ua.vodafone.entity.Tgno;

import java.util.List;

public interface TgnoRepository extends JpaRepository<Tgno, Long> {

    List<Tgno> findAllByOrderByIdAsc();

    @Query("select tgno.id from Tgno tgno where tgno.name=:name")
    Long getTgnoIdByName(@Param("name") String name);

    @Query("select tgno.name from Tgno tgno")
    List<String> getAllTgnoNames();

    Tgno getTgnoByName(String name);
}
