package ua.vodafone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.vodafone.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Customer getCustomerByUserName(String name);

    Customer getCustomerById(Long id);

}
